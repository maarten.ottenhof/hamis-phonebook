package com.hamis.phonebook.contact;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class ContactControllerTest {

    @MockBean
    private ContactService contactService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldGetAllContacts() throws Exception {
        when(contactService.getAllContacts())
                .thenReturn(List.of(new Contact(1L, "Henk", "Timmer", "06123456789", false)));

        mockMvc.perform(get("/contacts"))
                .andExpect(status().isOk());
    }

    @Test
    void shouldInsertContact() throws Exception {
        var contact = new Contact(null, "Ed", "Visser", "06123456789", false);
        when(contactService.insertContact(contact))
                .thenReturn(contact.withId(1L));

        mockMvc.perform(post("/contacts")
                .contentType(APPLICATION_JSON)
                .content("{\"firstName\": \"Ed\", \"lastName\": \"Visser\", \"phoneNumber\": \"06123456789\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.favourite").value(false));
    }

    @Test
    void shouldUpsertContact() throws Exception {
        var contact = new Contact(2L, "Ed", "Visser", "06123456789", false);
        when(contactService.upsertContact(contact))
                .thenReturn(contact.withId(2L));

        mockMvc.perform(put("/contacts/{id}", contact.getId())
                .contentType(APPLICATION_JSON)
                .content("{\"firstName\": \"Ed\", \"lastName\": \"Visser\", \"phoneNumber\": \"06123456789\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(contact.getId()))
                .andExpect(jsonPath("$.favourite").value(false));
    }

    @Test
    void shouldReturnErrorResponseWhenPhoneNumberIsNotUnique() throws Exception {
        var contact = new Contact(null, "Ed", "Visser", "06123456789", false);
        when(contactService.insertContact(contact))
                .thenThrow(new DataIntegrityViolationException("Unique constraint violated"));

        mockMvc.perform(post("/contacts")
                .contentType(APPLICATION_JSON)
                .content("{\"firstName\": \"Ed\", \"lastName\": \"Visser\", \"phoneNumber\": \"06123456789\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode").value("duplicate.phone.number"))
                .andExpect(jsonPath("$.errorMessage").value("A contact with the same phone number yet exists"));
    }

    @Test
    void shouldDeleteContact() throws Exception {
        var id = 1L;

        mockMvc.perform(delete("/contacts/{id}", id))
                .andExpect(status().isNoContent());

        verify(contactService).deleteContact(id);
    }

    @Test
    void shouldFlagContactAsFavourite() throws Exception {
        var id = 1L;

        mockMvc.perform(put("/contacts/{id}/favourite", id))
                .andExpect(status().isNoContent());

        verify(contactService).flagContactAsFavourite(id);
    }

    @Test
    void shouldUnflagContactAsFavourite() throws Exception {
        var id = 1L;

        mockMvc.perform(delete("/contacts/{id}/favourite", id))
                .andExpect(status().isNoContent());

        verify(contactService).unflagContactAsFavourite(id);
    }
}