package com.hamis.phonebook.contact;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContactServiceTest {

    @Mock
    private ContactRepository contactRepository;

    @InjectMocks
    private ContactService contactService;

    @Test
    void shouldGetAllContacts() {
        contactService.getAllContacts();

        verify(contactRepository).findAll();
    }

    @Test
    void shouldUnflagExistingFavouritesWhenInsertingNewFavourite() {
        var contact = new Contact(1L, "Ab", "Bakker", "06123456789", true);

        contactService.insertContact(contact);

        verify(contactRepository).unflagAllFavourites();
        verify(contactRepository).save(contact.withId(null));
    }

    @Test
    void shouldInsertContactWithoutUnflaggingFavouritesWhenContactIsNoFavourite() {
        var contact = new Contact(1L, "Ab", "Bakker", "06123456789", false);

        contactService.insertContact(contact);

        verify(contactRepository, never()).unflagAllFavourites();
        verify(contactRepository).save(contact.withId(null));
    }

    @Test
    void shouldUnflagExistingFavouritesWhenUpsertingWithNewFavourite() {
        var contact = new Contact(1L, "Ab", "Bakker", "06123456789", true);

        contactService.upsertContact(contact);

        verify(contactRepository).unflagAllFavourites();
        verify(contactRepository).save(contact);
    }

    @Test
    void shouldUpsertContactWithoutUnflaggingFavouritesWhenContactIsNoFavourite() {
        var contact = new Contact(1L, "Ab", "Bakker", "06123456789", false);

        contactService.upsertContact(contact);

        verify(contactRepository, never()).unflagAllFavourites();
        verify(contactRepository).save(contact);
    }

    @Test
    void shouldDeleteContactIfContactExists() {
        var id = 1L;
        when(contactRepository.existsById(id)).thenReturn(true);

        contactService.deleteContact(id);

        verify(contactRepository).deleteById(id);
    }

    @Test
    void shouldNotDeleteContactWhenNoContactFoundForId() {
        var id = 1L;
        when(contactRepository.existsById(id)).thenReturn(false);

        contactService.deleteContact(id);

        verifyNoMoreInteractions(contactRepository);
    }

    @Test
    void shouldFlagContactAsFavouriteIfContactExists() {
        var id = 1L;
        when(contactRepository.existsById(id)).thenReturn(true);

        contactService.flagContactAsFavourite(id);

        verify(contactRepository).unflagAllFavourites();
        verify(contactRepository).toggleFavouriteById(true, id);
    }

    @Test
    void shouldNotUnflagFavouritesIfContactToFlagAsFavouriteNotExists() {
        var id = 1L;
        when(contactRepository.existsById(id)).thenReturn(false);

        contactService.flagContactAsFavourite(id);

        verifyNoMoreInteractions(contactRepository);
    }

    @Test
    void shouldUnflagContactAsFavouriteById() {
        var id = 1L;

        contactService.unflagContactAsFavourite(id);

        verify(contactRepository).toggleFavouriteById(false, id);
    }
}