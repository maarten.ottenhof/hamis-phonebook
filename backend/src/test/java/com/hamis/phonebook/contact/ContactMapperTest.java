package com.hamis.phonebook.contact;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ContactMapperTest {

    @Test
    void shouldMapDtoToEntity() {
        var dto = new ContactDto(1L, "Ed", "Visser", "06123456789", false);

        var entity = ContactMapper.toEntity(dto);

        assertThat(entity).satisfies(actual -> {
            assertThat(actual.getId()).isEqualTo(dto.getId());
            assertThat(actual.getFirstName()).isEqualTo(dto.getFirstName());
            assertThat(actual.getLastName()).isEqualTo(dto.getLastName());
            assertThat(actual.getPhoneNumber()).isEqualTo(dto.getPhoneNumber());
            assertThat(actual.isFavourite()).isEqualTo(dto.isFavourite());
        });
    }

    @Test
    void shouldMapEntityToDto() {
        var entity = new Contact(1L, "Ed", "Visser", "06123456789", false);

        var dto = ContactMapper.toDto(entity);

        assertThat(dto).satisfies(actual -> {
            assertThat(actual.getId()).isEqualTo(entity.getId());
            assertThat(actual.getFirstName()).isEqualTo(entity.getFirstName());
            assertThat(actual.getLastName()).isEqualTo(entity.getLastName());
            assertThat(actual.getPhoneNumber()).isEqualTo(entity.getPhoneNumber());
            assertThat(actual.isFavourite()).isEqualTo(entity.isFavourite());
        });
    }
}