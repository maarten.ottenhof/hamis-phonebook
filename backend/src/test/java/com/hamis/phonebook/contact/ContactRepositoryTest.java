package com.hamis.phonebook.contact;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DataJpaTest
// Quick fix for test only working in isolation
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class ContactRepositoryTest {

    @Autowired
    private ContactRepository contactRepository;

    @Test
    void shouldSaveContact() {
        var contact = new Contact(null, "Ed", "Bakker", "0612345789", false);

        var storedContact = contactRepository.save(contact);

        var result = contactRepository.findById(storedContact.getId());
        assertThat(result).contains(storedContact);
    }

    @Test
    void shouldThrowOnDuplicatePhoneNumber() {
        var contact1 = new Contact(1L, "Ed", "Bakker", "0612345789", false);
        var contact2 = new Contact(2L, "Fred", "Timmer", "0612345789", false);

        contactRepository.save(contact1);

        assertThatThrownBy(() -> contactRepository.save(contact2)).isInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void shouldUnflagContactAsFavourite() {
        var contact = new Contact(1L, "Ed", "Bakker", "0612345789", true);
        contactRepository.save(contact);

        contactRepository.unflagAllFavourites();

        var result = contactRepository.findById(contact.getId());
        assertThat(result).get().satisfies(actual ->
            assertThat(actual.isFavourite()).isFalse()
        );
    }

    @Test
    void shouldFlagContactAsFavouriteById() {
        var contact = new Contact(1L, "Ed", "Bakker", "0612345789", false);
        contactRepository.save(contact);

        contactRepository.toggleFavouriteById(true, contact.getId());

        var result = contactRepository.findById(contact.getId());
        assertThat(result).get().satisfies(actual ->
            assertThat(actual.isFavourite()).isTrue()
        );
    }

    @Test
    void shouldUnflagContactAsFavouriteById() {
        var contact = new Contact(1L, "Ed", "Bakker", "0612345789", true);
        contactRepository.save(contact);

        contactRepository.toggleFavouriteById(false, contact.getId());

        var result = contactRepository.findById(contact.getId());
        assertThat(result).get().satisfies(actual ->
                assertThat(actual.isFavourite()).isFalse()
        );
    }

    @Test
    void shouldDoNothingWhenUnflaggingFavouriteThatDoesNotExist() {
        assertThatCode(() -> contactRepository.toggleFavouriteById(false, 42L))
                .doesNotThrowAnyException();
    }
}