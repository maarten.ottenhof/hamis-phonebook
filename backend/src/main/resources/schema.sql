DROP TABLE IF EXISTS contacts;

CREATE TABLE contacts
(
    id           BIGSERIAL PRIMARY KEY,
    first_name   VARCHAR(40) NOT NULL,
    last_name    VARCHAR(40),
    phone_number VARCHAR(15) NOT NULL UNIQUE,
    favourite    BOOLEAN     NOT NULL
)