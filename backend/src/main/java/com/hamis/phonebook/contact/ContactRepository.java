package com.hamis.phonebook.contact;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
interface ContactRepository extends JpaRepository<Contact, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Contact SET favourite = false WHERE favourite = true")
    void unflagAllFavourites();

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Contact SET favourite = :favourite WHERE id = :id")
    void toggleFavouriteById(boolean favourite, Long id);
}
