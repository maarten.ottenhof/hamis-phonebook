package com.hamis.phonebook.contact;

public abstract class ContactMapper {

    public static ContactDto toDto(Contact contact) {
        return new ContactDto(
                contact.getId(),
                contact.getFirstName(),
                contact.getLastName(),
                contact.getPhoneNumber(),
                contact.isFavourite()
        );
    }

    public static Contact toEntity(ContactDto contact) {
        return new Contact(
                contact.getId(),
                contact.getFirstName(),
                contact.getLastName(),
                contact.getPhoneNumber(),
                contact.isFavourite()
        );
    }
}
