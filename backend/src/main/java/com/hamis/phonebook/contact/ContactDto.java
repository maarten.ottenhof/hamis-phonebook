package com.hamis.phonebook.contact;

import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Value
public class ContactDto {
    Long id;
    @NotBlank(message = "First name cannot be empty")
    String firstName;
    String lastName;
    @Pattern(regexp = "^[+]*[(]?[\\d]{1,4}[)]?[-\\s\\d]*$", message = "Not a valid phone number")
    String phoneNumber;
    boolean favourite;
}
