package com.hamis.phonebook.contact;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/contacts")
@RequiredArgsConstructor
public class ContactController {

    private final ContactService contactService;

    @GetMapping
    public List<ContactDto> getAllContact() {
        return contactService.getAllContacts().stream()
                .map(ContactMapper::toDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ContactDto insertContact(@NotNull @Valid @RequestBody ContactDto contactDto) {
        var contact = ContactMapper.toEntity(contactDto);
        var storedContact = contactService.insertContact(contact);

        return ContactMapper.toDto(storedContact);
    }

    @PutMapping("/{id}")
    public ContactDto upsertContact(@NotNull @Valid @RequestBody ContactDto contactDto,
                                    @PathVariable("id") Long id) {
        var contact = ContactMapper.toEntity(contactDto);
        var storedContact = contactService.upsertContact(contact.withId(id));

        return ContactMapper.toDto(storedContact);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteContact(@PathVariable("id") Long id) {
        contactService.deleteContact(id);
    }

    @PutMapping("/{id}/favourite")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void flagContactAsFavourite(@PathVariable("id") Long id) {
        contactService.flagContactAsFavourite(id);
    }

    @DeleteMapping("/{id}/favourite")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void unflagContactAsFavourite(@PathVariable("id") Long id) {
        contactService.unflagContactAsFavourite(id);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorReponse handleDuplicateData(DataIntegrityViolationException e) {
        return new ErrorReponse(
                "duplicate.phone.number",
                "A contact with the same phone number yet exists"
        );
    }

    @Value
    static class ErrorReponse {
        String errorCode;
        String errorMessage;
    }
}
