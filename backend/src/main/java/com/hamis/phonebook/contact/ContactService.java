package com.hamis.phonebook.contact;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ContactService {

    private final ContactRepository contactRepository;

    public List<Contact> getAllContacts() {
        return contactRepository.findAll();
    }

    @Transactional
    public Contact insertContact(Contact contact) {
        // Since only one contact is allowed to be favourite at a time, unflag existing favourite
        if (contact.isFavourite()) {
            contactRepository.unflagAllFavourites();
        }
        return contactRepository.save(contact.withId(null));
    }

    @Transactional
    public Contact upsertContact(Contact contact) {
        // Since only one contact is allowed to be favourite at a time, unflag existing favourite
        if (contact.isFavourite()) {
            contactRepository.unflagAllFavourites();
        }
        return contactRepository.save(contact);
    }

    public void deleteContact(Long id) {
        if (contactRepository.existsById(id)) {
            contactRepository.deleteById(id);
        }
    }

    @Transactional
    public void flagContactAsFavourite(Long id) {
        // check if contact exists in order to prevent no favourite exists anymore when contact does not exist
        if (contactRepository.existsById(id)) {
            contactRepository.unflagAllFavourites();
            contactRepository.toggleFavouriteById(true, id);
        }
    }

    public void unflagContactAsFavourite(Long id) {
        contactRepository.toggleFavouriteById(false, id);
    }
}
