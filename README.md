# Hamis Phonebook
Telefoonboekapplicatie gemaakt in React + Spring Boot voor de HaMIS opdracht.

## Hoe de applicatie (frontend + backend) te starten
### Frontend + Backend
#### Eenmalig
1. `cd backend`
2. `mvn clean package`
3. `cd ..`

#### Applicatie starten
1. `docker-compose up`

De frontend is beschikbaar op `http://localhost:3000`
De api is beschikbaar op `http://localhost:8080/api/contacts`

### Backend
#### Backend starten
1.  `cd backend`
2. `docker-compose up`
   
De api is beschikbaar op `http://localhost:8080/api/contacts`


### Frontend
#### Frontend starten
1.  `cd frontend`
2. `docker-compose up`

De frontend is beschikbaar op `http://localhost:3000`

## Gemaakte aannames/keuzes
- Meest recente Java versie
    - Java 16 of Java 11, gekozen voor Java 16
- Gemiddelde gebruiker 50+
    - Overzichtelijke layout, functionaliteiten moeten duidelijk zichtbaar zijn
- Uitgegaan van één enkele gebruiker
- Telefoonnummer moet uniek zijn
- Geldig telefoonnummer is een string die voldoet aan de regex: `^[+]*[(]?[\d]{1,4}[)]?[-\s\d]*$`
- Het maken van een nieuw favoriet contact, onfavoriet de oude favoriet

## Eerste verbeterpunten in geval van meer tijd
-   Correcte validatie van ingevoerde gegevens in frontend en terugkoppeling over de validatie naar gebruiker
    -   Gebruik maken van een form library om dit te vereenvoudigen
-   Mogelijkheid om contact aan te passen, hierbij gebruikmakend van hetzelfde formulier als voor het aanmaken van een contact
-   Toevoegen van testen aan de frontend
-   Bevesting vragen bij het verwijderen van een contact
-   Toevoegen van verdere documentatie in de code
-   Uitzoeken waarom de `maven:alpine` image niet met java 16 lijkt te werken, of alternatieve maven image zoeken, om de backend te kunnen packagen in de Dockerfile.