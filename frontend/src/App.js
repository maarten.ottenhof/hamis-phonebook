import { QueryClient, QueryClientProvider } from "react-query";
import ContactList from "./components/ContactList";
import Header from "./components/Header";

const queryClient = new QueryClient();

const App = () => (
  <QueryClientProvider client={queryClient}>
    <Header />
    <ContactList />
  </QueryClientProvider>
);

export default App;
