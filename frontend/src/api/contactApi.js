import axios from "axios";

const BASE_URL = "http://localhost:8080/api";

const client = axios.create({
  baseURL: BASE_URL,
});

export const getContacts = async () => {
  const response = await client.get("/contacts");
  return response.data;
};

export const deleteContactById = (id) => {
  client.delete(`/contacts/${id}`);
};

export const postContact = async (contact) => {
  const response = await client.post("/contacts", contact);
  return response.data;
};

export const putContactAsFavourite = (id) => {
  client.put(`/contacts/${id}/favourite`);
};

export const deleteContactAsFavourite = (id) => {
  client.delete(`/contacts/${id}/favourite`);
};
