import {
  AppBar,
  Button,
  makeStyles,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { postContact } from "../api/contactApi";
import ContactFormDialog from "./ContactFormDialog";

const useStyles = makeStyles({
  toolbar: {
    justifyContent: "space-between",
  },
});

const Header = () => {
  const classes = useStyles();
  const queryClient = useQueryClient();
  const [showForm, setShowForm] = useState(false);

  const addContact = useMutation(postContact, {
    onSuccess: (storedContact) => {
      queryClient.setQueryData("contacts", (contacts) => [
        ...contacts,
        storedContact,
      ]);
    },
  });

  return (
    <>
      <ContactFormDialog
        title="Nieuw contact"
        open={showForm}
        handleClose={() => setShowForm(false)}
        handleSubmit={addContact.mutate}
      />
      <AppBar position="absolute" color="default">
        <Toolbar className={classes.toolbar}>
          <Typography variant="h6">Hamis Telefoonboek</Typography>
          <Button
            variant="contained"
            color="primary"
            onClick={() => setShowForm(true)}
            startIcon={<Add />}
          >
            Nieuw contact
          </Button>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Header;
