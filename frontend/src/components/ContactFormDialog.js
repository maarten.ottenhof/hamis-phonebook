import {
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Button,
  makeStyles,
  TextField,
} from "@material-ui/core";
import { Cancel, Save } from "@material-ui/icons";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  form: {
    "& .MuiTextField-root, & .MuiFormControlLabel-root": {
      margin: theme.spacing(1),
      width: "50ch",
    },
  },
}));

const emptyForm = {
  firstName: "",
  lastName: "",
  phoneNumber: "",
  favourite: false,
};

const ContactFormDialog = ({ title, open, handleClose, handleSubmit }) => {
  const classes = useStyles();
  const [formInput, setFormInput] = useState(emptyForm);

  const isBlank = (str) => {
    return !str || str.length === 0 || !str.trim();
  };

  const isPhoneNumber = (str) => {
    return !str || str.match("[^a-zA-Z]");
  };

  const handleChange = (e) => {
    setFormInput({ ...formInput, [e.target.name]: e.target.value });
  };

  const handleChecked = (e) => {
    setFormInput({ ...formInput, [e.target.name]: e.target.checked });
  };

  const validateFields = () => {
    if (isBlank(formInput.firstName) || !isPhoneNumber(formInput.phoneNumber)) {
      return false;
    }
    return true;
  };

  const onClose = () => {
    setFormInput(emptyForm);
    handleClose();
  };

  const onSave = () => {
    if (validateFields()) {
      handleSubmit(formInput);
      onClose();
    }
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <form className={classes.form}>
          <div>
            <TextField
              autoFocus
              required
              name="firstName"
              value={formInput.firstName}
              onChange={handleChange}
              label="Voornaam"
            />
          </div>
          <div>
            <TextField
              name="lastName"
              value={formInput.lastName}
              onChange={handleChange}
              label="Achternaam"
            />
          </div>
          <div>
            <TextField
              name="phoneNumber"
              required
              value={formInput.phoneNumber}
              onChange={handleChange}
              label="Telefoonnummer"
            />
          </div>
          <FormControlLabel
            control={
              <Checkbox
                name="favourite"
                checked={formInput.favourite}
                onClick={handleChecked}
                color="primary"
              />
            }
            label="Maak favoriet"
          />
        </form>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          color="primary"
          startIcon={<Save />}
          onClick={onSave}
        >
          Opslaan
        </Button>
        <Button
          variant="contained"
          color="secondary"
          startIcon={<Cancel />}
          onClick={onClose}
        >
          Annuleren
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ContactFormDialog;
