import { IconButton, TableCell, TableRow } from "@material-ui/core";
import { Delete, Star, StarBorder } from "@material-ui/icons";

const Contact = ({ onDelete, onFavourite, contact }) => {
  return (
    <TableRow key={contact.id}>
      <TableCell align="center">
        {contact.favourite ? (
          <IconButton onClick={() => onFavourite(false, contact.id)}>
            <Star color="primary" />
          </IconButton>
        ) : (
          <IconButton onClick={() => onFavourite(true, contact.id)}>
            <StarBorder />
          </IconButton>
        )}
      </TableCell>
      <TableCell>{contact.firstName}</TableCell>
      <TableCell>{contact.lastName}</TableCell>
      <TableCell align="right">{contact.phoneNumber}</TableCell>
      <TableCell align="right">
        <IconButton onClick={() => onDelete(contact.id)}>
          <Delete />
        </IconButton>
      </TableCell>
    </TableRow>
  );
};

export default Contact;
