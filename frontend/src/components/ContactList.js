import {
  CircularProgress,
  Container,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { useMutation, useQuery, useQueryClient } from "react-query";
import {
  deleteContactAsFavourite,
  deleteContactById,
  getContacts,
  putContactAsFavourite,
} from "../api/contactApi";
import Contact from "./Contact";

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  paper: {
    marginTop: 10,
  },
}));

const ContactList = () => {
  const classes = useStyles();
  const queryClient = useQueryClient();
  const { data: contacts, isLoading, isError } = useQuery(
    "contacts",
    getContacts
  );

  const deleteContact = useMutation(deleteContactById, {
    onSuccess: (data, id) => {
      queryClient.setQueryData("contacts", (oldContacts) =>
        oldContacts.filter((contact) => contact.id !== id)
      );
    },
  });

  const favouriteContact = useMutation(putContactAsFavourite, {
    onSuccess: (data, id) => {
      queryClient.setQueryData("contacts", (contacts) =>
        contacts.map((contact) =>
          contact.id === id
            ? { ...contact, favourite: true }
            : { ...contact, favourite: false }
        )
      );
    },
  });

  const unfavouriteContact = useMutation(deleteContactAsFavourite, {
    onSuccess: (data, id) => {
      queryClient.setQueryData("contacts", (contacts) =>
        contacts.map((contact) =>
          contact.id === id ? { ...contact, favourite: false } : contact
        )
      );
    },
  });

  const toggleFavourite = (favourite, id) => {
    favourite ? favouriteContact.mutate(id) : unfavouriteContact.mutate(id);
  };

  if (isError) {
    return <div>An error occured</div>;
  }

  return (
    <Container maxWidth="md">
      <div className={classes.toolbar} />
      <Paper className={classes.paper}>
        {isLoading ? (
          <CircularProgress />
        ) : (
          <Table>
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell>Voornaam</TableCell>
                <TableCell>Achternaam</TableCell>
                <TableCell align="right">Telefoonnummer</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {contacts.map((contact) => (
                <Contact
                  key={contact.id}
                  contact={contact}
                  onFavourite={toggleFavourite}
                  onDelete={deleteContact.mutate}
                />
              ))}
            </TableBody>
          </Table>
        )}
      </Paper>
    </Container>
  );
};

export default ContactList;
